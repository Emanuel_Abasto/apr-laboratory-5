package university.jala.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;
import university.jala.main.domain.Barber;
import university.jala.main.domain.Client;
import university.jala.main.repository.BarberRepository;
import university.jala.main.repository.ClientRepository;
import university.jala.main.services.Service;

public class ServiceTest {

  @Test
  public void AssignClientsTest() {
    Barber julio = BarberRepository.getInstance().getBarberList().get(0);
    Barber tulio = BarberRepository.getInstance().getBarberList().get(1);

    Service.assignClients();

    int julioClients = julio.getClientQueue().getClientQueue().size();
    int tulioClients = tulio.getClientQueue().getClientQueue().size();
    
    assertNotEquals(0, julioClients);
    assertNotEquals(0, tulioClients);
  }

  @Test
  public void makeProcessTest() {
    Service.assignClients();
    String messageExpected = "c1, EN_ESPERA\nc2, EN_ESPERA\nc3, EN_ESPERA\nc4, EN_ESPERA\nc5, EN_ESPERA\nc6, EN_ESPERA\nc7, EN_ESPERA\nc8, EN_ESPERA\n";
    assertEquals(messageExpected, makeMessage());

    Service.makeProcess();
    messageExpected = "c1, EN_ATENCION\nc2, EN_ATENCION\nc3, EN_ESPERA\nc4, EN_ESPERA\nc5, EN_ESPERA\nc6, EN_ESPERA\nc7, EN_ESPERA\nc8, EN_ESPERA\n";
    assertEquals(messageExpected, makeMessage());

    Service.makeProcess();
    messageExpected = "c1, SERVIDO\nc2, SERVIDO\nc3, EN_ATENCION\nc4, EN_ATENCION\nc5, EN_ESPERA\nc6, EN_ESPERA\nc7, EN_ESPERA\nc8, EN_ESPERA\n";
    assertEquals(messageExpected, makeMessage());

    Service.makeProcess();
    messageExpected = "c1, SERVIDO\nc2, SERVIDO\nc3, SERVIDO\nc4, SERVIDO\nc5, EN_ATENCION\nc6, EN_ATENCION\nc7, EN_ESPERA\nc8, EN_ESPERA\n";
    assertEquals(messageExpected, makeMessage());
  }

  private String makeMessage() {
    String message = "";
    for (Client client : ClientRepository.getInstance().getClientList()) {
      message += client.toString() + "\n";
    }
    return message;
  }
}
