package university.jala.main.repository;

import java.util.LinkedList;
import java.util.List;
import university.jala.main.domain.Client;
import university.jala.main.domain.ClientState;

public class ClientRepository {

  private int id = 0;
  private static ClientRepository instance;
  private List<Client> clientList = new LinkedList<>();

  private ClientRepository() {
    defaultClients();
  }

  public static ClientRepository getInstance() {
    if (instance == null) {
      instance = new ClientRepository();
    }
    return instance;
  }

  public List<Client> getClientList() {
    return clientList;
  }

  public void addClient(Client client) {
    clientList.add(client);
  }

  private int generateId() {
    return id++;
  }

  public List<Client> availableClients() {
    List<Client> clientsList = new LinkedList<>();
    for (Client client : getClientList()) {
      if (client.getClientState() == ClientState.EN_COLA) {
        clientsList.add(client);
      }
    }
    return clientsList;
  }

  public void defaultClients() {
    addClient(new Client("c1", generateId()));
    addClient(new Client("c2", generateId()));
    addClient(new Client("c3", generateId()));
    addClient(new Client("c4", generateId()));
    addClient(new Client("c5", generateId()));
    addClient(new Client("c6", generateId()));
    addClient(new Client("c7", generateId()));
    addClient(new Client("c8", generateId()));
  }
}
