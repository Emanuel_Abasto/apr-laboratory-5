package university.jala.main.repository;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import university.jala.main.domain.Barber;
import university.jala.main.domain.BarberState;

public class BarberRepository {

  private static BarberRepository instance;
  private List<Barber> barberList = new LinkedList<>();
  private int id = 0;

  private BarberRepository() {
    defaultBarbers();
  }

  public static BarberRepository getInstance() {
    if (instance == null) {
      instance = new BarberRepository();
    }
    return instance;
  }

  public List<Barber> getBarberList() {
    return barberList;
  }

  public void addBarber(Barber barber) {
    barberList.add(barber);
  }

  public List<Barber> availableBarbers() {
    List<Barber> availableBarbers = new ArrayList<>();
    for (Barber barber : barberList) {
      if (barber.getBarberState() != BarberState.RESTING && barber.getClientQueue().isAvailable()) {
        availableBarbers.add(barber);
      }
    }
    return sortList(availableBarbers);
  }

  private int generateID() {
    return id++;
  }

  private void defaultBarbers() {
    addBarber(new Barber("Julio", generateID(), BarberState.FREE));
    addBarber(new Barber("Tulio", generateID(), BarberState.FREE));
  }

  public List<Barber> sortList(List<Barber> list) {
    for (int index = 1; index < list.size(); index++) {
      Barber currentBarber = list.get(index);
      int previousBarberIndex = index - 1;

      while (previousBarberIndex >= 0
          && list.get(previousBarberIndex).getClientQueue().getClientQueue().size()
          > currentBarber.getClientQueue().getClientQueue().size()) {
        list.set(previousBarberIndex + 1, list.get(previousBarberIndex));
        previousBarberIndex--;
      }
      list.set(previousBarberIndex + 1,currentBarber);
    }
    return list;
  }
}
