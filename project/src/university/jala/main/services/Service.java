package university.jala.main.services;

import java.util.List;
import university.jala.main.domain.Barber;
import university.jala.main.domain.BarberState;
import university.jala.main.domain.Client;
import university.jala.main.repository.BarberRepository;
import university.jala.main.repository.ClientRepository;

public class Service {

  private static BarberRepository barberRepository = BarberRepository.getInstance();
  private static ClientRepository clientRepository = ClientRepository.getInstance();

  private static void assignClient() {
    List<Barber> barbers = barberRepository.availableBarbers();
    if (!barbers.isEmpty() && !clientRepository.availableClients().isEmpty()) {
      barbers.get(0).getClientQueue().addClient(clientRepository.availableClients().get(0));
    }
  }

  public static void assignClients() {
    while (true) {
      List<Barber> barberList = barberRepository.availableBarbers();
      List<Client> clientList = clientRepository.availableClients();
      if (barberList.size() == 0 || clientList.size() == 0) {
        break;
      }
      assignClient();
    }
  }

  public static void makeProcess() {
    for (Barber barber : barberRepository.getBarberList()) {
      if (barber.getBarberState() != BarberState.RESTING) {
        barber.process();
      }
    }
  }
}
