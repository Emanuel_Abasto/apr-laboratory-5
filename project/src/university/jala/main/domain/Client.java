/**
 * The `Client` class represents a client entity with a name, ID, and client state. It provides
 * methods to retrieve and modify client information, such as name and state.
 */
package university.jala.main.domain;

import java.util.Objects;

public class Client {

  private String name;
  private int id;
  private ClientState clientState;

  /**
   * Constructs a new `Client` instance with the given name and ID.
   * The client state is set to 'EN_ESPERA' (Waiting) by default.
   *
   * @param name The name of the client.
   * @param id   The unique ID assigned to the client.
   */
  public Client(String name, int id) {
    this.name = name;
    this.id = id;
    clientState = ClientState.EN_COLA;
  }

  /**
   * Retrieves the name of the client.
   *
   * @return The name of the client.
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the name of the client.
   *
   * @param name The new name for the client.
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Retrieves the ID of the client.
   *
   * @return The ID of the client.
   */
  public int getId() {
    return id;
  }

  /**
   * Sets the ID of the client.
   *
   * @param id The new ID for the client.
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * Retrieves the state of the client.
   *
   * @return The current state of the client.
   */
  public ClientState getClientState() {
    return clientState;
  }

  /**
   * Sets the state of the client.
   *
   * @param clientState The new state for the client.
   */
  public void setClientState(ClientState clientState) {
    this.clientState = clientState;
  }

  /**
   * Compares this `Client` instance to another object for equality.
   *
   * @param o The object to compare with this client.
   * @return `true` if the objects are equal, `false` otherwise.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Client client = (Client) o;
    return id == client.id && Objects.equals(name, client.name);
  }

  /**
   * Generates a hash code for this `Client` instance.
   *
   * @return The hash code for this client.
   */
  @Override
  public int hashCode() {
    return Objects.hash(name, id);
  }

  @Override
  public String toString() {
    return name + ", " + clientState;
  }
}
