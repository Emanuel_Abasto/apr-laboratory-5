package university.jala.main.domain;

public enum BarberState {
  ASSISTING,
  RESTING,
  FREE
}
