package university.jala.main.domain;

public enum ClientState {
  EN_ESPERA,
  EN_ATENCION,
  SERVIDO,
  EN_COLA
}
