package university.jala.main.domain;

import java.util.LinkedList;
import java.util.Queue;

public class ClientQueue {
  private Queue<Client> clientQueue = new LinkedList<>();
  private int limit = 5;

  public Queue<Client> getClientQueue() {
    return clientQueue;
  }

  public void setClientQueue(Queue<Client> clientQueue) {
    this.clientQueue = clientQueue;
  }

  public int getLimit() {
    return limit;
  }

  public void setLimit(int limit) {
    this.limit = limit;
  }

  public boolean isAvailable() {
    return clientQueue.size() < limit;
  }

  public boolean addClient(Client client) {
    if (isAvailable() && client != null && client.getClientState() == ClientState.EN_COLA) {
      client.setClientState(ClientState.EN_ESPERA);
      clientQueue.add(client);
      return true;
    }
    return false;
  }
}
