/**
 * The `Barber` class represents a barber entity with a name, ID, state, and client queue. It
 * manages clients waiting for service in a queue and has a maximum queue limit.
 */
package university.jala.main.domain;

import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;

public class Barber {

  private String name;           // The name of the barber.
  private int id;                // The unique ID assigned to the barber.
  private BarberState barberState; // The current state of the barber.
  private ClientQueue clientQueue; // Queue of clients waiting for service.
  // Maximum number of clients in the queue.

  /**
   * Constructs a new `Barber` instance with the specified name, ID, and initial barber state.
   * Initializes an empty client queue and sets the default queue limit to 5.
   *
   * @param name        The name of the barber.
   * @param id          The unique ID assigned to the barber.
   * @param barberState The initial state of the barber.
   */
  public Barber(String name, int id, BarberState barberState) {
    this.name = name;
    this.id = id;
    this.barberState = barberState;
    clientQueue = new ClientQueue();
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public BarberState getBarberState() {
    return barberState;
  }

  public void setBarberState(BarberState barberState) {
    this.barberState = barberState;
  }

  public ClientQueue getClientQueue() {
    return clientQueue;
  }

  public void setClientQueue(ClientQueue clientQueue) {
    this.clientQueue = clientQueue;
  }

  /**
   * Compares this `Barber` instance to another object for equality.
   *
   * @param o The object to compare with this barber.
   * @return `true` if the objects are equal, `false` otherwise.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Barber barber = (Barber) o;
    return id == barber.id && Objects.equals(name, barber.name);
  }

  /**
   * Generates a hash code for this `Barber` instance.
   *
   * @return The hash code for this barber.
   */
  @Override
  public int hashCode() {
    return Objects.hash(name, id);
  }

  private void bringService() {
    barberState = BarberState.ASSISTING;
    clientQueue.getClientQueue().peek().setClientState(ClientState.EN_ATENCION);
  }

  private void finishService() {
    barberState = BarberState.FREE;
    clientQueue.getClientQueue().peek().setClientState(ClientState.SERVIDO);
    clientQueue.getClientQueue().poll();
    if (clientQueue.getClientQueue().peek() != null) {
      clientQueue.getClientQueue().peek().setClientState(ClientState.EN_ATENCION);
    }
  }

  public void process() {
    if (clientQueue.getClientQueue().peek() != null) {
      if (clientQueue.getClientQueue().peek().getClientState() == ClientState.EN_ATENCION) {
        finishService();
      } else if (clientQueue.getClientQueue().peek().getClientState() == ClientState.EN_ESPERA) {
        bringService();
      }
    }
  }
}
